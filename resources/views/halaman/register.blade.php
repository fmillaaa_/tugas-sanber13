@extends('layout.master')
@section('judul')
Buat Account Baru!
@endsection

@section('content')
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="post">
        @csrf
        <label>First Name:</label><br><br>
        <input type="text" name="firstName"><br><br>
        <label>Last Name:</label><br><br>
        <input type="text" name="lastName"><br><br>
        <label>Gender:</label><br><br>
        <input type="radio" name="gender" value="1">Male<br>
        <input type="radio" name="gender" value="2">Female<br>
        <input type="radio" name="gender" value="3">Other<br><br>
        <label>Nationality:</label><br><br>
        <select name="nationality">
            <option value="1">Indonesia</option>br
            <option value="2">Malaysia</option>br
            <option value="3">Singapura</option>br
            <option value="4">Australia</option>br
        </select><br><br>
        <label>Language Spoken:</label><br><br>
        <input type="checkbox" name="language" value="1">Bahasa Indonesia<br>
        <input type="checkbox" name="language" value="2">English<br>
        <input type="checkbox" name="language" value="3">Other<br><br>
        <label>Bio:</label><br><br>
        <textarea name="bio" cols="25" rows="15"></textarea><br><br>
        <button type="submit">Sign Up</button>
    </form>
@endsection
